#!/bin/bash

set -e

if [ $# -lt 1 ]; then
    echo "[USEAGE] $0 {config_file}"
    exit 
fi

IMAGE="t6"
name=$(basename "$1")
if [ "$2" == "devel" ]; then
    DEVEL="1"
    name="${name}_devel"
fi

echo make device $name

if [ "$DEVEL" == "1" ]; then
    DEVEL_APP="$PWD/../devel/$name/app"
    if [ -e $DEVEL_APP ]; then
        echo "already exit $DEVEL_APP"
    else
        mkdir -p $DEVEL_APP
        (cd $DEVEL_APP && \
         git clone --recursive git@gitlab.testbed.systems:iot/device-virtual-instance.git && 
         cd device-virtual-instance && 
         npm install)
    fi
    docker create -v $DEVEL_APP:/app --name "$name" $IMAGE
else
    docker create --name "$name" $IMAGE
fi

DEVICE_ID=$(cat "$1" | grep "deviceId" | sed -e "s/^[^:]*://" -e "s/[ \",]//g")

mkdir -p store
sed -e "s/__DEVICE_UUID__/$DEVICE_ID/" template/_info.dev.json > store/info.dev.json

if [ "$DEVEL" == "1" ]; then
    cp -a store $DEVEL_APP/device-virtual-instance/
else
    fakeroot docker cp store $name:/app/device-virtual-instance/
fi

rm -rf store

docker diff $name
