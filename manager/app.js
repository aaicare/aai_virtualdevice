var express = require('express'),
  http = require('http'),
  app = express(),
  bodyParser = require('body-parser'),
  server = http.createServer(app);

app.use(bodyParser.urlencoded({
  extended: true
}));
app.set('view engine', 'pug')

var vm_m = require('./lib/vm_state');
var vm_manager = new vm_m();
vm_manager.update(function() {
  // console.log(vm_manager.machines);
});

setInterval(function() {
  vm_manager.update(function() {
    // console.log(vm_manager.machines);
  });
}, 60 * 1000);

app.get('/', function(req, res) {
  show(res);
});

app.post('/op', function(req, res) {
  console.log(req.url, req.method, req.body);
  var action = req.body.action;
  var uuid = req.body.uuid;
  if (action == "start") {
    vm_manager.start(uuid, function() {
      console.log("started")
      show(res);
    });
  } else if (action == "stop") {
    vm_manager.stop(uuid, function() {
      console.log("stopped")
      show(res);
    });
  } else {
    console.log("invalid op:", action);
    show(res);
  }
});

app.post('/pop', function(req, res) {
  console.log(req.url, req.method, req.body);
  var action = req.body.action;
  var uuid = req.body.uuid;
  var pollutant = req.body.pollutant;
  var new_value = req.body.value;
  if (action == "set") {
    vm_manager.set_pollutant(uuid, pollutant, new_value, function() {
      console.log("set")
      show(res);
    });
  } else if (action == "auto") {
    vm_manager.auto_pollutant(uuid, pollutant, new_value, function() {
      console.log("auto")
      show(res);
    });
  } else if (action == "stop") {
    vm_manager.stop_pollutant(uuid, pollutant, new_value, function() {
      console.log("stop")
      show(res);
    });
  } else {
    console.log("invalid op:", action);
    show(res);
  }
});

server.listen(9857, function() {
  console.log('Express server listening on port ' + server.address().port);
});

function show(res) {
  res.render('index', {
    virtual_machines: vm_manager.machines
  });
};
