var async = require('async');
var exec = require('child_process').exec;

function update_all(callback) {
  update_containers(function(err, machines) {
    async.mapSeries(machines, update_vm_state, callback);
  });
};

function update_containers(callback) {
  exec('docker ps -a --filter ancestor=t6 --format="{{.Names}}\t{{.Status}}"', function(err, stdout, stderr) {
    var lines = stdout.trim().split(/\r|\n/).sort();
    var machines = lines.map(function(l) {
      var values = l.trim().split('\t');
      var active = (values[1].search("Exited") == -1) && (values[1].search("Created") == -1);
      return {
        "name": values[0],
        "serialnumber": values[0],
        "state": values[1],
        "active": active ? "Active" : "Sleeping"
      };
    });
    callback(err, machines);
  });
};

function update_vm_state(machine, callback) {
	var tmpfile = 'tmp/' + machine.name;
	exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/info.dev.json ' + tmpfile 
			+ ' && cat ' + tmpfile 
			+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
		try {
			var infos = JSON.parse(stdout)
			async.series({
				uuid: function(callback) {
					callback(null, infos.device.uuid);
				},
				server_address: function(callback) {
					callback(null, infos.server.address);
				},
				server_port: function(callback) {
					callback(null, infos.server.port);
				},
				pollutant_current: function(callback) {
					update_current_value(machine, callback);
				},
				pollutant_state: function(callback) {
					update_current_state(machine, callback);
				},
				logs: function(callback) {
					exec('docker logs ' + machine.name, function(err, stdout, stderr) {
						callback(null, stdout);
					});
				}
				
			},
			function(err, results) {
				Object.assign(machine, results);
				callback(null, machine)
				console.log(machine.name)
			});
		}
		catch(e) {
			console.log(e)
			callback(null, stdout);
		};
	});
};

function update_current_value(machine, callback) {
	var tmpfile = 'tmp/' + machine.name;
	exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/current.txt ' + tmpfile 
			+ ' && cat ' + tmpfile 
			+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
		if (stdout.length == 0) {
			callback(null, stdout);
		}
		else {
			try {
				var currents = JSON.parse(stdout);
				async.series({
					co2: function(callback) {
						callback(null, parseInt(currents.co2).toString());
					},
					no2: function(callback) {
						callback(null, parseInt(currents.no2).toString());
					},
					pm: function(callback) {
						callback(null, parseInt(currents.pm).toString());
					},
					vocs: function(callback) {
						callback(null, parseInt(currents.vocs).toString());
					},
					temperature: function(callback) {
						callback(null, parseInt(currents.temperature).toString());
					},
					humidity: function(callback) {
						callback(null, parseInt(currents.humidity).toString());
					}
				},
				function(err, results) {
					Object.assign(machine, results);
					callback(null, machine)
					console.log(machine.name)
				});
			}
			catch(e) {
				console.log(e)
				callback(null, stdout);
			};
		}
	});
};

function update_current_state(machine, callback) {
	async.series({
		co2s: function(callback) {
			var tmpfile = 'tmp/' + machine.name + 'co2s';
			exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/co2 ' + tmpfile 
					+ ' && cat ' + tmpfile 
					+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
				callback(null, (stdout.length > 0) ? ((parseInt(stdout) == -1) ? 'stop' : stdout) : 'auto');
			});
		},
		no2s: function(callback) {
			var tmpfile = 'tmp/' + machine.name + 'no2s';
			exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/no2 ' + tmpfile 
					+ ' && cat ' + tmpfile 
					+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
				callback(null, (stdout.length > 0) ? ((parseInt(stdout) == -1) ? 'stop' : stdout) : 'auto');
			});
		},
		pms: function(callback) {
			var tmpfile = 'tmp/' + machine.name + 'pms';
			exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/pm ' + tmpfile 
					+ ' && cat ' + tmpfile 
					+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
				callback(null, (stdout.length > 0) ? ((parseInt(stdout) == -1) ? 'stop' : stdout) : 'auto');
			});
		},
		vocss: function(callback) {
			var tmpfile = 'tmp/' + machine.name + 'vocss';
			exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/vocs ' + tmpfile 
					+ ' && cat ' + tmpfile 
					+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
				callback(null, (stdout.length > 0) ? ((parseInt(stdout) == -1) ? 'stop' : stdout) : 'auto');
			});
		},
		temperatures: function(callback) {
			var tmpfile = 'tmp/' + machine.name + 'temperatures';
			exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/temperature ' + tmpfile 
					+ ' && cat ' + tmpfile 
					+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
				callback(null, (stdout.length > 0) ? ((parseInt(stdout) == -1) ? 'stop' : stdout) : 'auto');
			});
		},
		humiditys: function(callback) {
			var tmpfile = 'tmp/' + machine.name + 'humiditys';
			exec('docker cp ' + machine.name + ':/app/device-virtual-instance/store/humidity ' + tmpfile 
					+ ' && cat ' + tmpfile 
					+ ' && rm ' + tmpfile, function(err, stdout, stderr) {
				callback(null, (stdout.length > 0) ? ((parseInt(stdout) == -1) ? 'stop' : stdout) : 'auto');
			});
		}
	},
	function(err, results) {
		Object.assign(machine, results);
		callback(null, machine)
		console.log(machine.name)
	});
};

////////////////////////////////////////////////////////////////////////////////

function VirtualMachineManager() {
  this.machines = [];
}

VirtualMachineManager.prototype.update = function(callback) {
  var _this = this;
  update_all(function(err, results) {
    _this.machines = results;
    callback();
  });
};

VirtualMachineManager.prototype.stop = function(name, callback) {
  var _this = this;
  exec('docker stop ' + name, function(err, stdout, stderr) {
    if (err != null) {
      console.log("err: " + err);
    }
    if (stderr != null) {
      console.log("stderr: " + stderr);
    }
    _this.update(callback);
  });
};

VirtualMachineManager.prototype.start = function(name, callback) {
  var _this = this;
  exec('docker start ' + name, function(err, stdout, stderr) {
    if (err != null) {
      console.log("err: " + err);
    }
    if (stderr != null) {
      console.log("stderr: " + stderr);
    }
    _this.update(callback);
  });
};

VirtualMachineManager.prototype.set_pollutant = function(name, pollutant, new_value, callback) {
  var _this = this;
  var tmpfile = 'tmp/' + pollutant;
  exec('echo ' + new_value.toString() + ' >> ' + tmpfile 
               + ' && docker cp ' + tmpfile + ' ' + name + ':/app/device-virtual-instance/store/' + pollutant 
               + ' && rm ' + tmpfile, function(err, stdout, stderr) {
    if (err != null) {
      console.log("err: " + err);
    }
    if (stderr != null) {
      console.log("stderr: " + stderr);
    }
    _this.update(callback);
  });
};

VirtualMachineManager.prototype.stop_pollutant = function(name, pollutant, new_value, callback) {
  var _this = this;
  var tmpfile = 'tmp/' + pollutant;
  exec('echo -1 >> ' + tmpfile 
               + ' && docker cp ' + tmpfile + ' ' + name + ':/app/device-virtual-instance/store/' + pollutant 
               + ' && rm ' + tmpfile, function(err, stdout, stderr) {
    if (err != null) {
      console.log("err: " + err);
    }
    if (stderr != null) {
      console.log("stderr: " + stderr);
    }
    _this.update(callback);
  });
};

VirtualMachineManager.prototype.auto_pollutant = function(name, pollutant, new_value, callback) {
  var _this = this;
  var tmpfile = 'tmp/' + pollutant;
  exec('docker exec ' + name + ' rm store/' + pollutant, function(err, stdout, stderr) {
    if (err != null) {
      console.log("err: " + err);
    }
    if (stderr != null) {
      console.log("stderr: " + stderr);
    }
    _this.update(callback);
  });
};

module.exports = VirtualMachineManager;
